<h1 align="center">Welcome to Churras Trinca Front 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000" />
  <img alt="License: MIT" src="https://img.shields.io/badge/License-MIT-yellow.svg" />
  ![Netlify Status](https://api.netlify.com/api/v1/badges/a6f27990-ab38-447d-af18-59912b856e27/deploy-status)
</p>

> Repositório do Front-end da agenda de churras

### ✨ [Demo]

https://churrastrinca.netlify.com/

## Install

```sh
yarn install
```

## Usage

```sh
yarn run start
```

## Author

👤 **Matheus Brondani**

* Github: [@https:\/\/gitlab.com\/matt.brondani](https://github.com/https:\/\/gitlab.com\/matt.brondani)
* LinkedIn: [@https:\/\/www.linkedin.com\/in\/matheus-brondani-de-oliveira-9b0603107\/](https://linkedin.com/in/https:\/\/www.linkedin.com\/in\/matheus-brondani-de-oliveira-9b0603107\/)

## Show your support

Give a ⭐️ if this project helped you!

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_
