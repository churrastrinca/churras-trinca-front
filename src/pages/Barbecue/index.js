import React from 'react';

import { Container, Content } from './styles';
import Header from '../../components/Header';
import BarbecueForm from '../../components/BarbecueForm';
import Footer from '../../components/Footer';

export default function Barbecue() {
  return (
    <Container>
      <Header />
      <Content>
        <BarbecueForm />
      </Content>
      <Footer />
    </Container>
  );
}
