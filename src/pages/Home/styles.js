import styled from 'styled-components';

export const Container = styled.div`
  height: 100%;
  overflow: auto;
`;

export const Content = styled.div`
  min-height: 60%;
  background-color: #FFF;
  display: flex;
  flex-direction: column;
`;
