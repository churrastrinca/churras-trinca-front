import React from 'react';

import { Container, Content } from './styles';
import BarbecueList from '../../components/BarbecueList';
import Header from '../../components/Header';
import Footer from '../../components/Footer';

export default function Home() {
  return (
    <Container>
      <Header />
      <Content>
        <BarbecueList />
      </Content>
      <Footer />
    </Container>
  )
}
