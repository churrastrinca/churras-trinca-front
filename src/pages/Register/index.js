import React from 'react';

import { Container, Content, Header, Title, Footer, Logo } from './styles';
import RegisterForm from '../../components/RegisterForm';

export default function Register() {
  return (
    <Container>
      <Header>
        <Title>Agenda de Churras</Title>
      </Header>
      <Content>
        <RegisterForm />
      </Content>
      <Footer>
        <Logo />
      </Footer>
    </Container>
  );
}
