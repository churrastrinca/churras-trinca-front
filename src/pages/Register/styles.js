import styled from 'styled-components';
import logo from '../../assets/trincaLogo.png';

export const Container = styled.div`
  height: 100%;
`;

export const Content = styled.div`
  height: 70%;
`;

export const Footer = styled.div`
  height: 10%;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const Logo = styled.div`
  background-image: url(${logo});
  width: 48px;
  height: 48px;
`;

export const Header = styled.div`
  height: 20%;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  align-items: center;
  justify-content: center;
`;

export const Title = styled.h1`
  font-size: 32px;
  font-weight: 880;
  line-height: 37px;
`;
