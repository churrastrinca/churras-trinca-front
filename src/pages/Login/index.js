import React from 'react';

import { Container, Content, Footer, Logo, Header, Title } from './styles';
import LoginForm from '../../components/LoginForm';

export default function Login() {
  return (
    <Container>
      <Header>
        <Title>Agenda de Churras</Title>
      </Header>
      <Content>
        <LoginForm />
      </Content>
      <Footer>
        <Logo />
      </Footer>
    </Container>
  );
}
