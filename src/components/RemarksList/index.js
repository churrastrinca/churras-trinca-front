import React, { useState } from 'react';
import { toast } from 'react-toastify';

import {
  RemarkDesc,
  Container,
  Remark,
  Remove,
  Input,
  List,
} from './styles';

export default function RemarksList(props) {
  const [additionalRemarks, setAdditionalRemarks] = useState('');
  const { additionalRemarksList, setAdditionalRemarksList } = props;

  function handlePressEnter(e) {
    if (e.key === 'Enter') {
      e.preventDefault();
      if (additionalRemarks.trim() === "") {
        toast.info('Preencha o campo de observção antes de inserir');
        return;
      }
      setAdditionalRemarksList([...additionalRemarksList, e.target.value]);
      setAdditionalRemarks('');
    }
  }

  function handleRemoveRemark(e, remark) {
    e.preventDefault();
    const newList = additionalRemarksList.filter(x => x !== remark);
    setAdditionalRemarksList(newList);
  }

  return (
    <Container>
      <Input
        onChange={e => setAdditionalRemarks(e.target.value)}
        onKeyDown={e => handlePressEnter(e)}
        placeholder="Observações adicionais - digite e aperte enter"
        value={additionalRemarks}
        type="additionalRemarks"
        id="additionalRemarks"
      />

      <List>
        {
          additionalRemarksList.map((remark, i) => {
            return (
              <Remark key={i}>
                <RemarkDesc>{remark}</RemarkDesc>
                <Remove onClick={e => { handleRemoveRemark(e, remark) }}>X</Remove>
              </Remark>
            )
          })
        }
      </List>
    </Container>
  )
}
