import styled from 'styled-components';

export const Container = styled.div``;

export const Input = styled.input`
  width: 100%;
  height: 48px;
  font-size: 16px;
  padding: 0 20px;
  margin-bottom: 30px;
`;

export const Button = styled.button`
  height: 48px;
  background: #000000;
  border: 1px solid #ddd;
  border-radius: 18px;
  font-size: 16px;
  padding: 0 20px;
  margin-top: 10px;
  color: #FFF;
  font-weight: bold;
  border: 0;
  cursor: pointer;
`;

export const List = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  margin-bottom: 15px;
`;

export const Remark = styled.div`
  border-bottom: 1px solid #E5C231;
  margin-bottom: 5px;
  margin-right: 10px;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-between;
`;

export const RemarkDesc = styled.div`
  margin-right: 10px;
`;

export const Remove = styled.div`
  font-size: 16px;
  cursor: pointer;
`;
