import styled from 'styled-components';

export const Container = styled.div`
  width: 40%;
  padding: 40px;
  border: 3px;
  border-color: black;
  box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.06);
  background-color: #FFF;
  margin-top: -30px;
  margin-bottom: 20px;

  @media (max-width: 1440px) {
    width: 60%;
  }

  @media (max-width: 920px) {
    width: 75%;
  }

  @media (max-width: 720px) {
    width: 90%;
  }
`;

export const Form = styled.form`
  display: flex;
  justify-content: center;
  flex-direction: column;
`;

export const Label = styled.label`
  font-size: 21px;
  font-weight: bold;
  color: #000000;
  margin-bottom: 20px;
`;

export const Input = styled.input`
  height: 48px;
  font-size: 16px;
  padding: 0 20px;
  margin-bottom: 30px;
`;

export const Button = styled.button`
  height: 48px;
  background: #000000;
  border: 1px solid #ddd;
  border-radius: 18px;
  font-size: 16px;
  padding: 0 20px;
  margin-top: 10px;
  color: #FFF;
  font-weight: bold;
  border: 0;
  cursor: pointer;
`;
