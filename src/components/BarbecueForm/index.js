import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { toast } from 'react-toastify';
import Loader from 'react-loader-spinner';
import moment from 'moment';

import { postBarbecue, getBarbecue, putBarbecue } from '../../Services/barbecueRequests';
import ParticipantsDrop from '../../components/ParticipantsDrop';
import RemarksList from '../../components/RemarksList';
import { Container, Button, Input, Form } from './styles';
import { getId } from '../../Services/auth';

function BarbecueForm({ history, match }) {
  const [date, setDate] = useState('');
  const [description, setDescription] = useState('');
  const [participants, setParticipants] = useState([]);
  const [additionalRemarksList, setAdditionalRemarksList] = useState([]);
  const [loading, setLoading] = useState(false);
  const [editing, setEditing] = useState(false);
  const [barbecueId, setBarbecueId] = useState('false');

  useEffect(() => {
    async function loadBarbecue() {
      const id = match.params.barbecueId;
      if (id !== 'null') {
        setBarbecueId(id);
        setEditing(true);
        await getBarbecue(
          {
            barbecueId: id,
          },
          res => {
            if (res) {
              setDate(moment(res.data.date).format("YYYY-MM-DD"))
              setDescription(res.data.description)
              setAdditionalRemarksList(res.data.additionalRemarks);
              setParticipants(
                res.data.participants.map(x => ({
                  value: x.id,
                  label: x.name,
                  contribution: x.contribution,
                  willDrink: x.willDrink,
                  drinkContribution: x.drinkContribution,
                }))
              );
            }
          }
        );
      }
    }

    loadBarbecue();
  }, [match]);

  function handleParticipants() {

    if (participants.length < 1) {
      toast.info('É necessario inserir um participante!');
      return false;
    }

    return true;
  }

  async function handleSubmit(e) {
    e.preventDefault();
    setLoading(true);
    if (handleParticipants()) {
      await postBarbecue(
        {
          description,
          date,
          additionalRemarks: additionalRemarksList,
          creatorUser: getId(),
          participants: participants.map(p => ({
            id: p.value,
            name: p.label,
            contribution: p.contribution,
            willDrink: p.willDrink,
            drinkContribution: p.willDrink ? p.drinkContribution : 0,
          })),
        },
        res => {
          toast.success('Churrasco inserido com sucesso!');
          if (res) history.push('/Home');
        }
      );
    }
    setLoading(false);
  }

  async function handleSubmitEditing(e) {
    e.preventDefault();
    setLoading(true);
    if (handleParticipants()) {
      await putBarbecue(
        {
          id: barbecueId,
          description,
          date,
          additionalRemarks: additionalRemarksList,
          creatorUser: getId(),
          participants: participants.map(p => ({
            id: p.value,
            name: p.label,
            contribution: p.contribution,
            willDrink: p.willDrink,
            drinkContribution: p.willDrink ? p.drinkContribution : 0,
          })),
        },
        res => {
          toast.success('Churrasco alterado com sucesso!');
          if (res) history.push('/Home');
        }
      );
    }
    setLoading(false);
  }

  return (
    <Container>
      <Form onSubmit={editing ? handleSubmitEditing : handleSubmit}>
        <Input
          onChange={e => setDescription(e.target.value)}
          placeholder="Descrição"
          value={description}
          type="description"
          id="description"
          required
        />
        <Input
          onChange={e => setDate(e.target.value)}
          placeholder="Data"
          value={date}
          type="date"
          id="date"
          required
        />

        <RemarksList
          additionalRemarksList={additionalRemarksList}
          setAdditionalRemarksList={setAdditionalRemarksList}
        />

        <ParticipantsDrop
          participants={participants}
          setParticipants={setParticipants}
        />

        <Button type="submit" disabled={loading}>
          {loading &&
            <Loader
              type="Puff"
              color="#FFF"
              height={25}
              width={25}
            />
          }
          {!loading && (editing ? 'Salvar' : 'Adicionar')}
        </Button>
        <Button onClick={(e) => {
          e.preventDefault();
          history.push('/Home');
        }}>
          Voltar
        </Button>
      </Form>
    </Container>
  );
}

export default withRouter(BarbecueForm);
