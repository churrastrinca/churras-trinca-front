import React, { useState, useEffect, memo } from 'react';
import { withRouter } from 'react-router-dom';
import moment from 'moment';

import { getId } from '../../Services/auth';
import getBarbecueList from '../../Services/barbecueRequests';
import {
  AddBarbecueCard,
  PeopleCounter,
  Contributions,
  BarbecueData,
  BarbecueCard,
  Description,
  PeopleLogo,
  Container,
  CoinLogo,
  BbqLogo,
  AddText,
  Circle,
  Date,
} from './styles';

function BarbecueList({ history }) {
  const [barbecueList, setBarbecueList] = useState([]);

  useEffect(() => {
    async function loadBarbecueList() {
      await getBarbecueList(
        {
          id: getId(),
        },
        res => {
          if (res) {
            setBarbecueList(res.data);
          }
        }
      );
    }

    loadBarbecueList();
  }, []);

  function getTotalContributions(barbecue) {
    let totalContributions = 0;
    barbecue.participants.forEach(x => {
      totalContributions += x.contribution;
      if (x.willDrink) totalContributions += x.drinkContribution;
    });
    return totalContributions;
  }

  return (
    <Container>
      {
        barbecueList.map((barbecue, i) => {
          return (
            <BarbecueCard key={i} onClick={() => history.push(`/BarbecueDetails/${barbecue.id}`)}>
              <Date>{moment(barbecue.date).format('DD/MM')}</Date>
              <Description>{barbecue.description}</Description>
              <BarbecueData>
                <PeopleCounter>
                  <PeopleLogo />
                  {barbecue.participants.length}
                </PeopleCounter>
                <Contributions>
                  <CoinLogo />
                  R$ {getTotalContributions(barbecue)}
                </Contributions>
              </BarbecueData>
            </BarbecueCard>
          )
        })
      }
      <AddBarbecueCard onClick={() => history.push(`/Barbecue/${null}`)}>
        <Circle>
          <BbqLogo />
        </Circle>
        <AddText>Adicionar Churrasco</AddText>
      </AddBarbecueCard>
    </Container>
  );
}

export default memo(withRouter(BarbecueList));
