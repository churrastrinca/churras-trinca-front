import styled from 'styled-components';
import bbq from '../../assets/icon_bbq.png';
import people from '../../assets/icon_people.png';
import coin from '../../assets/icon_money.png';

export const Container = styled.div`
  margin-top: -30px;
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  flex-direction: row;
  flex-wrap: wrap;
`;

export const Button = styled.button`
  height: 48px;
  background: #000000;
  border: 1px solid #ddd;
  border-radius: 18px;
  font-size: 16px;
  padding: 0 20px;
  margin-top: 10px;
  color: #FFF;
  font-weight: bold;
  border: 0;
  cursor: pointer;
`;

export const BarbecueCard = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  height: 180px;
  width: 270px;
  box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.06);
  padding: 0 25px;
  border: 3px;
  border-color: black;
  background-color: #FFF;
  margin-bottom: 20px;
  cursor: pointer;
`;

export const BarbecueData = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  margin-top: 30px;
`;

export const PeopleCounter = styled.div`
  width: 50%;
  display: flex;
  justify-content: flex-start;
  flex-direction: row;
  flex-wrap: wrap;
  font-size: 20px;
`;

export const PeopleLogo = styled.div`
  background-image: url(${people});
  width: 18px;
  height: 15px;
  margin-right: 5px;
`;

export const Contributions = styled.div`
  width: 50%;
  display: flex;
  justify-content: flex-end;
  flex-direction: row;
  flex-wrap: wrap;
  font-size: 20px;
`;

export const CoinLogo = styled.div`
  background-image: url(${coin});
  width: 20px;
  height: 20px;
  margin-right: 5px;
`;

export const Description = styled.h2``;

export const Date = styled.h1``;

export const AddBarbecueCard = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  height: 180px;
  width: 270px;
  padding: 20px;
  border: 3px;
  border-color: black;
  background-color: #F1F1F1;
  margin-bottom: 20px;
  box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.06);
  cursor: pointer;
`;

export const BbqLogo = styled.div`
  background-image: url(${bbq});
  width: 48px;
  height: 48px;
`;

export const Circle = styled.div`
  border-radius: 50%;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  width: 100px;
  height: 100px;
  background: #FFD836;
  margin-bottom: 20px;
`;

export const AddText = styled.h2`
  font-style: normal;
  font-weight: bold;
  line-height: 25px;
  text-align: center;
`;
