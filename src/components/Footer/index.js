import React from 'react';
import { Container, Logo } from './styles';

export default function Footer() {
  return (
    <Container>
      <Logo />
    </Container>
  );
}
