import styled from 'styled-components';
import logo from '../../assets/trincaLogo.png';

export const Container = styled.div`
  height: 10%;
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: #FFF;
`;

export const Logo = styled.div`
  background-image: url(${logo});
  width: 48px;
  height: 48px;
`;
