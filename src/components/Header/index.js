import React from 'react';
import { withRouter } from 'react-router-dom';

import { logout, getName } from '../../Services/auth';
import { Container, Title, UserName, Button, Menu } from './styles';

function Header({ history }) {

  function handleLogout() {
    logout();
    history.push("/");
  }

  return (
    <Container>
      <Title>Agenda de Churras</Title>
      <Menu>
        <UserName>{getName()}</UserName>
        <Button onClick={handleLogout}> - Sair</Button>
      </Menu>
    </Container>
  );
}

export default withRouter(Header);
