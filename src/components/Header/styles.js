import styled from 'styled-components';

export const Container = styled.div`
  height: 30%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export const Title = styled.h1`
  font-size: 32px;
  font-weight: 880;
  line-height: 37px;
`;

export const Menu = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: row;
  flex-wrap: wrap;
  margin-top: 5px;
`;

export const UserName = styled.h3`
  margin-right: 5px;
`;

export const Button = styled.h3`
  font-weight: bold;
  cursor: pointer;
`;
