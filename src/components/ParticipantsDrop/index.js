import React, { useEffect, useState } from 'react';
import Select from 'react-select';

import { userList } from '../../Services/userRequests';
import {
  DrinkContribution,
  ParticipantsList,
  ParticipantLine,
  ParticipantHead,
  ParticipantName,
  Contribution,
  Container,
  WillDrink,
  Remove,
  Drink,
  Name,
  Coin,
} from './styles';

export default function ParticipantsDrop(props) {
  const [options, setOptions] = useState();
  const [removedOptions, setRemovedOptions] = useState(false);
  const { participants, setParticipants } = props;

  const customStyles = {
    control: (provided, state) => ({
      ...provided,
      borderRadius: 0,
    }),
  }

  useEffect(() => {
    async function loadUserList() {
      await userList(
        {
          id:'sdfdf',
        },
        res => {
          if (res.data) {
            const opt = res.data.map(x => ({
              value: x.id,
              label: x.name,
              contribution: 20,
              willDrink: false,
              drinkContribution: 20,
            }));
            setOptions(opt);
          }
        }
      );
    }

    loadUserList();
  }, []);

  useEffect(() => {
    function removeExistingOptions() {
      if(removedOptions)
        return false;

      if (participants.length > 0) {
        if (options) {
          setRemovedOptions(true);
          const opt = options.filter(opt => {
            const result = participants.find(op => {
              if (op.value === opt.value) return op
              return false;
            });
            if (!result) return opt;
            return false;
          });
          setOptions(opt);
        }
      }
    }

    removeExistingOptions();
  }, [participants, options, removedOptions]);

  function handleSelectChange(selected) {
    const opt = options.filter(x => x.value !== selected.value);
    setOptions(opt);
    setParticipants([...participants, selected]);
  }

  function handleRemoveParticipant(e, selected) {
    e.preventDefault();
    const part = participants.filter(x => x.value !== selected.value);
    setParticipants(part);
    setOptions([...options, selected]);
  }

  function handleContributionFields(value, participant, key, field) {
    const oldParticipant = participants.find(x => x.value === participant.value);

    participants.splice(key, 1, {
      ...oldParticipant,
      [field]: value,
    });

    setParticipants([...participants]);
  }

  return (
    <Container>
      <Select
        options={options}
        onChange={handleSelectChange}
        styles={customStyles}
      />

      <ParticipantsList>
        <ParticipantHead>
          <Name>Nome</Name>
          <Coin>R$</Coin>
          <Drink>Ceva ?</Drink>
          <Coin>R$</Coin>
          <Remove>X</Remove>
        </ParticipantHead>
        {
          participants.map((participant, i) => {
            return (
              <ParticipantLine key={i}>
                <ParticipantName>{participant.label}</ParticipantName>
                <Contribution
                  onChange={
                    e =>
                      handleContributionFields(
                        parseInt(e.target.value, 10),
                        participant,
                        i,
                        "contribution"
                      )
                  }
                  defaultValue={participant.contribution}
                  id="contribution"
                  type="number"
                  required
                  min={0}
                />
                <WillDrink
                  onChange={
                    e => handleContributionFields(e.target.checked, participant, i, "willDrink")
                  }
                  defaultChecked={participant.willDrink}
                  type="checkbox"
                />
                <DrinkContribution
                  parse={Number}
                  onChange={
                    e =>
                      handleContributionFields(
                        parseInt(e.target.value, 10),
                        participant,
                        i,
                        "drinkContribution"
                      )
                  }
                  defaultValue={participant.drinkContribution}
                  disabled={!participant.willDrink}
                  id="drinkContribution"
                  type="number"
                  required
                  min={0}
                />
                <Remove onClick={e => handleRemoveParticipant(e, participant)}>X</Remove>
              </ParticipantLine>
            )
          })
        }
      </ParticipantsList>
    </Container>
  )
}
