import styled from 'styled-components';

export const Container = styled.div`
  margin-bottom: 20px;
`;

export const ParticipantsList = styled.ul``;

export const ParticipantHead = styled.li`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  flex-wrap: wrap;
  font-size: 21px;
  width: 100%;
  margin-top: 20px;
  border-bottom: 1px solid #E5C231;
`;

export const ParticipantLine = styled.li`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  flex-wrap: wrap;
  font-size: 21px;
  width: 100%;
  margin-top: 20px;
  border-bottom: 1px solid #E5C231;
`;

export const DrinkContribution = styled.input`
  width: 20%;
  font-size: 16px;
  border: none;
`;

export const ParticipantName = styled.div`
  width: 40%;
`;

export const Contribution = styled.input`
  width: 20%;
  font-size: 16px;
  border: none;
`;

export const WillDrink = styled.input`
  width: 15%;
`;

export const Remove = styled.div`
  width: 5%;
  font-size: 16px;
  margin-top: 10px;
  cursor: pointer;
`;

export const Drink = styled.div`
  width: 15%;
`;

export const Name = styled.div`
  width: 40%;
`;

export const Coin = styled.div`
  width: 20%;
`;
