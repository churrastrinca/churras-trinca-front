import React, { useState, useEffect, memo } from 'react';
import moment from 'moment';
import { toast } from 'react-toastify';
import Loader from 'react-loader-spinner';

import Header from '../../components/Header';
import Footer from '../../components/Footer';
import { getBarbecue } from '../../Services/barbecueRequests';
import { deleteBarbecue } from '../../Services/barbecueRequests';
import {
  ParticipantsList,
  ParticipantsLine,
  ButtonContainer,
  PeopleCounter,
  Contributions,
  Contribution,
  BarbecueCard,
  RemarkTitle,
  RemarksList,
  Description,
  PeopleLogo,
  Container,
  CoinLogo,
  Content,
  Button,
  Space,
  Name,
  Date,
} from './styles';

function BarbecueDetails({ history, match }) {
  const [barbecue, setBarbecue] = useState('');
  const [participantsList, setParticipantsList] = useState([]);
  const [remarksList, setRemarksList] = useState([]);
  const [contributions, setContributions] = useState(0);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    async function loadBarbecue() {
      const barbecueId = match.params.barbecueId;
      await getBarbecue(
        {
          barbecueId,
        },
        res => {
          if (res) {
            setBarbecue(res.data);
            setRemarksList(res.data.additionalRemarks);
            setParticipantsList(res.data.participants);

            let totalContributions = 0.0;
            res.data.participants.forEach(x => {
              totalContributions += x.contribution;
              if (x.willDrink) totalContributions += x.drinkContribution;
            });
            setContributions(totalContributions);
          }
        }
      );
    }

    loadBarbecue();
  }, [match]);

  function getTotalContribution(participant) {
    let valueTotal = 0.0;
    valueTotal += participant.contribution
    if (participant.willDrink) valueTotal += participant.drinkContribution
    return valueTotal;
  }

  async function handleDelete(e) {
    e.preventDefault();
    if (window.confirm('Tem certeza q quer deletar este churrasco?')) {
      setLoading(true);
      await deleteBarbecue(
        {
          id: barbecue.id,
        },
        res => {
          toast.success('Churrasco deletado com sucesso!');
          if (res) history.push('/Home');
        }
      );
      setLoading(false);
    }
  }

  return (
    <Container>
      <Header />
      <Content>
        {
          <BarbecueCard>
            <Date>{moment(barbecue.date).format('DD/MM')}</Date>
            <PeopleCounter>
              <PeopleLogo />
              {participantsList.length}
            </PeopleCounter>
            <Description>{barbecue.description}</Description>
            <Contributions>
              <CoinLogo />
              R$ {contributions}
            </Contributions>
            {remarksList.length > 0 &&
              < RemarksList >
                <RemarkTitle>
                  Observações Adicionais
              </RemarkTitle>
                {
                  remarksList.map((remark) => `${remark}; `)
                }
              </RemarksList>
            }
            <ParticipantsList>
              {
                participantsList.map((participant, i) => {
                  return (
                    <ParticipantsLine key={i}>
                      <Space />
                      <Name>{participant.name}</Name>
                      <Contribution>R$ {getTotalContribution(participant)}</Contribution>
                    </ParticipantsLine>
                  )
                })
              }
            </ParticipantsList>
            <ButtonContainer>
              <Button onClick={() => history.push('/Home')}>Voltar</Button>
              <Button onClick={handleDelete}>
                {loading &&
                  <Loader
                    type="Puff"
                    color="#FFF"
                    height={25}
                    width={25}
                  />
                }
                {!loading && 'Deletar'}
              </Button>
              <Button onClick={() => history.push(`/Barbecue/${barbecue.id}`)}>Editar</Button>
            </ButtonContainer>
          </BarbecueCard>
        }
      </Content>
      <Footer />
    </Container >
  );
}

export default memo(BarbecueDetails);
