import styled from 'styled-components';
import people from '../../assets/icon_people.png';
import coin from '../../assets/icon_money.png';

export const Container = styled.div`
  height: 100%;
  overflow: auto;
`;

export const Content = styled.div`
  background-color: #FFF;
  display: flex;
  align-items: center;
  flex-direction: column;
`;

export const BarbecueCard = styled.div`
  width: 40%;
  padding: 40px;
  display: flex;
  justify-content: center;
  flex-direction: row;
  flex-wrap: wrap;
  border: 3px;
  border-color: black;
  box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.06);
  background-color: #FFF;
  margin-top: -30px;
  margin-bottom: 20px;

  @media (max-width: 1440px) {
    width: 60%;
  }

  @media (max-width: 920px) {
    width: 75%;
  }

  @media (max-width: 720px) {
    width: 90%;
  }
`;

export const PeopleCounter = styled.div`
  width: 20%;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: flex-start;
  font-size: 20px;
  margin-top: 5px;
`;

export const PeopleLogo = styled.div`
  background-image: url(${people});
  width: 18px;
  height: 15px;
  margin-right: 8px;
`;

export const Contributions = styled.div`
  width: 20%;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: flex-start;
  font-size: 20px;
  margin-top: 5px;
`;

export const CoinLogo = styled.div`
  background-image: url(${coin});
  width: 20px;
  height: 20px;
  margin-right: 5px;
`;

export const Description = styled.div`
  width: 80%;
  font-size: 40px;
  line-height: 42px;
`;

export const Date = styled.div`
  width: 80%;
  font-weight: 800;
  font-size: 28px;
  line-height: 33px;
`;

export const RemarksList = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  margin-top: 20px;
`;

export const RemarkTitle = styled.h2`
  margin-bottom: 5px;
`;

export const ParticipantsList = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  width: 100%;
  margin-top: 30px;
`;

export const ParticipantsLine = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  font-weight: bold;
  font-size: 21px;
  line-height: 25px;
  width: 100%;
  margin-top: 20px;
  border-bottom: 1px solid #E5C231;
`;

export const Space = styled.div`
  width: 10%;
`;

export const Name = styled.div`
  width: 70%;
`;

export const Contribution = styled.div`
  width: 20%;
`;

export const ButtonContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
  flex-wrap: wrap;
`;

export const Button = styled.button`
  width: 20%;
  height: 48px;
  background: #000000;
  border: 1px solid #ddd;
  border-radius: 18px;
  font-size: 16px;
  padding: 0 20px;
  margin-top: 20px;
  color: #FFF;
  font-weight: bold;
  border: 0;
  cursor: pointer;
`;
