import React, { useState } from 'react';
import { withRouter } from 'react-router-dom';
import { toast } from 'react-toastify';
import Loader from 'react-loader-spinner';

import authenticate from '../../Services/userRequests';
import { Button, Container, Form, Label, Input } from './styles';
import { login } from '../../Services/auth';

function LoginForm({ history }) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);

  async function handleSubmit(e) {
    e.preventDefault();
    setLoading(true);
    await authenticate(
      { email, password },
      res => {
        toast.success('Login efetuado com sucesso!');
        if (res) {
          login(res.data);
          history.push('/Home');
        }
      }
    );
    setLoading(false);
  }

  return (
    <Container>
      <Form onSubmit={handleSubmit}>
        <Label htmlFor="email">E-mail</Label>
        <Input
          onChange={e => setEmail(e.target.value)}
          placeholder="e-mail"
          value={email}
          type="email"
          id="email"
          required
        />
        <Label htmlFor="password">Senha</Label>
        <Input
          onChange={e => setPassword(e.target.value)}
          placeholder="senha"
          value={password}
          type="password"
          id="password"
          required
        />
        <Button type="submit" disabled={loading}>
          {loading &&
            <Loader
              type="Puff"
              color="#FFF"
              height={25}
              width={25}
            />
          }
          {!loading && "Entrar"}
        </Button>
        <Button
          disabled={loading}
          onClick={e => {
            e.preventDefault();
            history.push('/Register');
          }}
        >
          Registrar-se
        </Button>
      </Form>
    </Container>
  );
}

export default withRouter(LoginForm);
