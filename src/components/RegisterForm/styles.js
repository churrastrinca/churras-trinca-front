import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  justify-content: center;
`;

export const Form = styled.form`
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

export const Label = styled.label`
  font-size: 21px;
  font-weight: bold;
  color: #000000;
  margin-bottom: 20px;
`;

export const Input = styled.input`
  width: 282px;
  height: 50px;
  font-size: 18px;
  padding: 0 20px;
  margin-bottom: 30px;
  ::placeholder, ::-webkit-input-placeholder {
    font-style: italic;
  }
  box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.06);
  border: none;
  border-radius: 2px;
`;

export const Button = styled.button`
  height: 48px;
  background: #000000;
  border: 1px solid #ddd;
  border-radius: 18px;
  font-size: 16px;
  padding: 0 20px;
  margin-top: 10px;
  color: #FFF;
  font-weight: bold;
  border: 0;
  cursor: pointer;
`;
