import React, { useState } from 'react';
import { withRouter } from 'react-router-dom';
import { toast } from 'react-toastify';
import Loader from 'react-loader-spinner';

import { registerUser } from '../../Services/userRequests';
import { Container, Form, Input, Button } from './styles';

function RegisterForm({ history }) {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmEmail, setConfirmEmail] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [loading, setLoading] = useState(false);

  function compareFields() {

    if (!(email === confirmEmail)) {
      toast.info('Os campos de e-mail não estão iguais.');
      return false;
    }

    if (!(password === confirmPassword)) {
      toast.info('Os campos de senha não estão iguais.');
      return false;
    }

    return true;
  }

  async function handleSubmit(event) {
    event.preventDefault();
    if (compareFields()) {
      setLoading(true);
      await registerUser(
        { name, email, password },
        res => {
          if (res) {
            toast.success('Registro efetuado com sucesso!');
            history.push('/')
          }
        }
      );
      setLoading(false);
    }
  }

  return (
    <Container>
      <Form onSubmit={handleSubmit}>
        <Input
          onChange={event => setName(event.target.value)}
          placeholder="Nome"
          value={name}
          id="name"
          required
        />
        <Input
          onChange={event => setEmail(event.target.value)}
          placeholder="E-mail"
          value={email}
          type="email"
          id="email"
          required
        />
        <Input
          onChange={event => setConfirmEmail(event.target.value)}
          placeholder="Confirme o e-mail"
          value={confirmEmail}
          id="confirmEmail"
          type="email"
          required
        />
        <Input
          onChange={event => setPassword(event.target.value)}
          placeholder="Senha"
          value={password}
          type="password"
          id="password"
          required
        />
        <Input
          onChange={event => setConfirmPassword(event.target.value)}
          placeholder="Confirme a senha"
          value={confirmPassword}
          type="password"
          id="confirmPassword"
          required
        />
        <Button type="submit" disabled={loading}>
          {loading &&
            <Loader
              type="Puff"
              color="#FFF"
              height={25}
              width={25}
            />
          }
          {!loading && "Cadastrar"}
        </Button>
        <Button
          disabled={loading}
          onClick={e => {
            e.preventDefault();
            history.push('/');
          }}
        >
          Voltar
        </Button>
      </Form>
    </Container>
  );
}

export default withRouter(RegisterForm);
