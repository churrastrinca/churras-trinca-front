const hostname = window && window.location && window.location.hostname;

const BASE_URL =
    hostname === 'localhost'
    ? 'https://localhost:5001/'
    : 'https://churrastrincaapi.herokuapp.com/';

export default BASE_URL;
