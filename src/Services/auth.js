export const isAuthenticated = () => {
  const user = JSON.parse(localStorage.getItem('user'));
  return user && user.token ? true : false;
};

export const getToken = () => {
  const user = JSON.parse(localStorage.getItem('user'));
  return user.token || ' ';
};

export const getId = () => {
  const user = JSON.parse(localStorage.getItem('user'));
  return user.id || ' ';
};

export const getName = () => {
  const user = JSON.parse(localStorage.getItem('user'));
  return user.name || ' ';
};

export const login = user => {
  localStorage.setItem('user',JSON.stringify(user));
};

export const logout = () => {
  localStorage.removeItem('user');
};
