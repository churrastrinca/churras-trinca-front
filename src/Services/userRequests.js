import request from './api';
import { getToken } from './auth';

export default async function authenticate(payload, result) {
  return await request(
    {
      method: 'post',
      url: 'authenticate',
    },
    {
      ...payload,
    },
    result
  );
}

export async function registerUser(payload, result) {
  return await request(
    {
      method: 'post',
      url: 'register',
    },
    {
      ...payload,
    },
    result
  );
}

export async function userList(payload, result) {
  return await request(
    {
      method: 'get',
      url: 'api/user',
      headers: { 'Authorization': `Bearer ${getToken()}` },
    },
    {
      ...payload,
    },
    result
  );
}
