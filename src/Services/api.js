import axios from 'axios';
import { toast } from 'react-toastify';
import BASE_URL from './config';

const api = axios.create({
  baseURL: BASE_URL,
});

async function request(query, payload, result) {
  return await api({
    method: query.method,
    url: query.url,
    headers: query.headers,
    data: { ...payload },
  })
    .then(res => result(res))
    .catch(error => {
      if (error.response.status === 404) toast.warn(error.response.data.message);
      if (error.response.status === 400) {
        if (error.response.data.message) {
          toast.warn(error.response.data.message);
          return;
        }
        if (error.response.data.title) {
          toast.warn(error.response.data.title);
          return;
        }
      }
      if (error.response.status === 500) toast.error(error.response.data.message);
    });
}

export default request;
