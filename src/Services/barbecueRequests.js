import request from './api';
import { getToken } from './auth';

export default async function getBarbecueList(payload, result) {
  return await request(
    {
      method: 'get',
      url: `/api/barbecue?userId=${payload.id}`,
      headers: { 'Authorization': `Bearer ${getToken()}` },
    },
    {
      ...payload,
    },
    result
  );
}

export async function postBarbecue(payload, result) {
  return await request(
    {
      method: 'post',
      url: 'api/barbecue',
      headers: { 'Authorization': `Bearer ${getToken()}` },
    },
    {
      ...payload,
    },
    result
  );
}

export async function getBarbecue(payload, result) {
  return await request(
    {
      method: 'get',
      url: `/barbecue?barbecueId=${payload.barbecueId}`,
      headers: { 'Authorization': `Bearer ${getToken()}` },
    },
    {
      ...payload,
    },
    result
  );
}

export async function putBarbecue(payload, result) {
  return await request(
    {
      method: 'put',
      url: `api/barbecue?barbecueId=${payload.id}`,
      headers: { 'Authorization': `Bearer ${getToken()}` },
    },
    {
      ...payload,
    },
    result
  );
}

export async function deleteBarbecue(payload, result) {
  return await request(
    {
      method: 'delete',
      url: `api/barbecue?id=${payload.id}`,
      headers: { 'Authorization': `Bearer ${getToken()}` },
    },
    {
      ...payload,
    },
    result
  );
}
