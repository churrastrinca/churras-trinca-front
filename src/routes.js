import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';

import Login from './pages/Login';
import Register from './pages/Register';
import Home from './pages/Home';
import Barbecue from './pages/Barbecue';
import BarbecueDetails from './components/BarbecueDetails';
import { isAuthenticated } from "./Services/auth";

const AuthenticatedRoute = ({ component: Component, ...rest }) => (
  <Route {...rest}
    render={(props) => (
      isAuthenticated() ? (
        <Component {...props} path={props.path} />
      ) :
        (
          <Redirect to={{ pathname: "/", state: { from: props.location } }} />
        )
    )}
  />
);

export default function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Login} />
        <Route path="/Register" component={Register} />
        <AuthenticatedRoute path="/Home" component={Home} />
        <AuthenticatedRoute path="/Barbecue/:barbecueId" component={Barbecue} />
        <AuthenticatedRoute path="/BarbecueDetails/:barbecueId" component={BarbecueDetails} />
        <Route path="*" component={() => <h1>Page not found</h1>} />
      </Switch>
    </BrowserRouter>
  );
}
